package es.josemiguel.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "com.animelib.dao");
        schema.enableKeepSectionsByDefault();
        addRss(schema);
        new DaoGenerator().generateAll(schema, "app/src-gen/main/java");
    }

    private static void addRss(Schema schema) {
        Entity rss = schema.addEntity("Rss");
        rss.addStringProperty("title");
        rss.addStringProperty("link").primaryKey();
        rss.addStringProperty("description");
        rss.addDateProperty("publishDate");
        rss.addStringProperty("cover");
    }

}

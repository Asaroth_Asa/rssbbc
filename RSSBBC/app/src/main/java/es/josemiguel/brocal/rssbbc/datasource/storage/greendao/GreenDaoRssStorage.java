package es.josemiguel.brocal.rssbbc.datasource.storage.greendao;

import java.util.List;

import javax.inject.Inject;

import es.josemiguel.brocal.rssbbc.datasource.model.Rss;
import es.josemiguel.brocal.rssbbc.datasource.storage.RssStorage;

/**
 * Persiste Rss en una base de datos SQLite utilizando el ORM: GreenDao.
 * <p/>
 * Created by josemiguel on 09/04/14.
 */
public class GreenDaoRssStorage implements RssStorage {

    private RssDao rssDao;

    @Inject
    public GreenDaoRssStorage(RssDao rssDao) {
        this.rssDao = rssDao;
    }

    @Override
    public void persistRss(List<Rss> rss) {
        rssDao.insertOrReplaceInTx(rss, false);
    }

    @Override
    public List<Rss> getAllRss() {
        return rssDao.loadAll();
    }

    @Override
    public List<Rss> getAllRssOrderByPublishDate() {
        return rssDao.queryBuilder().orderAsc(RssDao.Properties.PublishDate).list();
    }
}

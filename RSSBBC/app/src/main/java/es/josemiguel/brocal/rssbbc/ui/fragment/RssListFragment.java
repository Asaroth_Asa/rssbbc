package es.josemiguel.brocal.rssbbc.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import es.josemiguel.brocal.rssbbc.R;
import es.josemiguel.brocal.rssbbc.datasource.api.RssApi;
import es.josemiguel.brocal.rssbbc.datasource.model.Rss;
import es.josemiguel.brocal.rssbbc.ui.adapter.RssListAdapter;
import es.josemiguel.brocal.rssbbc.ui.base.BaseFragment;
import es.josemiguel.brocal.rssbbc.ui.viewmodel.RssListViewModel;
import rx.Observer;
import rx.Subscription;

public class RssListFragment extends BaseFragment {

    public static final String TAG = "rssListfragment";

    @Inject
    RssApi rssApi;
    @Inject
    Picasso picasso;
    @Inject
    RssListViewModel viewModel;

    private ListView rssListView;

    private RssListAdapter adapter;

    private OnRssSelectedListener onRssSelectedListener;

    private int lastRssSelected = 0;

    private Observer<List<Rss>> populateObserver = new Observer<List<Rss>>() {
        @Override
        public void onCompleted() {
        }

        @Override
        public void onError(Throwable e) {
            Toast.makeText(getActivity(), "Ups.. " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(List<Rss> args) {
            adapter.swapRss(args);
        }
    };

    private Subscription subscription;

    public RssListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            setHasOptionsMenu(true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rssListView = (ListView) inflater.inflate(R.layout.fragment_rss_list, container, false);
        initView(rssListView);
        return rssListView;
    }

    private void initView(ListView rssListView) {
        rssListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (onRssSelectedListener != null) {
                    lastRssSelected = position;
                    onRssSelectedListener.onRssSelected(adapter.getItem(lastRssSelected));
                }
            }
        });
        adapter = new RssListAdapter(new ArrayList<Rss>(), picasso);
        rssListView.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onRssSelectedListener = (OnRssSelectedListener) activity;
        } catch (ClassCastException ignored) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        subscription = viewModel.getRssFromCache(populateObserver);
    }

    @Override
    public void onPause() {
        subscription.unsubscribe();
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            refreshData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshData() {
        adapter.swapRss(new ArrayList<Rss>());
        subscription = viewModel.populateRss(populateObserver);
    }

    public interface OnRssSelectedListener {
        void onRssSelected(Rss rss);
    }
}

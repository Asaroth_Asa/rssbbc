package es.josemiguel.brocal.rssbbc.datasource.api;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import es.josemiguel.brocal.rssbbc.datasource.model.Rss;

/**
 * Interfaz para consumir la "api" de Rss.
 */
public interface RssApi {

    List<Rss> getRss() throws IOException, XmlPullParserException;

}

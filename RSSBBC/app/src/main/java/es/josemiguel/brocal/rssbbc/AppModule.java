package es.josemiguel.brocal.rssbbc;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.josemiguel.brocal.rssbbc.datasource.module.DataSourceModule;
import es.josemiguel.brocal.rssbbc.datasource.storage.greendao.GreenDaoModule;
import es.josemiguel.brocal.rssbbc.domain.module.DomainModule;
import es.josemiguel.brocal.rssbbc.ui.UIModule;

@Module(
        library = true
)
public final class AppModule {

    private App application;

    public AppModule(App application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication() {
        return application;
    }

    public static final class Modules {

        public static Object[] list(App app) {
            return new Object[]{
                    new AppModule(app),
                    new UIModule(),
                    new GreenDaoModule(),
                    new DomainModule(),
                    new DataSourceModule()
            };
        }

        private Modules() {
            // No instances.
        }
    }

}

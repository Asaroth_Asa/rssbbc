package es.josemiguel.brocal.rssbbc.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import es.josemiguel.brocal.rssbbc.R;
import es.josemiguel.brocal.rssbbc.datasource.model.Rss;
import es.josemiguel.brocal.rssbbc.ui.fragment.RssDetailFragmet;
import es.josemiguel.brocal.rssbbc.ui.fragment.RssListFragment;


public class MainActivity extends ActionBarActivity implements RssListFragment.OnRssSelectedListener {

    private static final String IS_HANDSET = "ishandset";
    /**
     * Determina si el dispositivo es un movil (pantalla menor de 7")
     */
    private boolean isHandset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) {
            if (findViewById(R.id.main_frag_rss_list) != null) {
                initForTablets();
                isHandset = false;
            } else {
                initForHandset();
                isHandset = true;
            }
        } else {
            isHandset = savedInstanceState.getBoolean(IS_HANDSET);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_HANDSET, isHandset);
    }

    private void initForTablets() {

    }

    private void initForHandset() {
        RssListFragment rssListFragment = new RssListFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.activity_main_container, rssListFragment, RssListFragment.TAG)
                .commit();
    }

    @Override
    public void onRssSelected(Rss rss) {
        if (isHandset) {
            RssDetailFragmet rssDetailFragmet = RssDetailFragmet.newInstance(rss.getLink());
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.activity_main_container, rssDetailFragmet, RssDetailFragmet.TAG)
                    .addToBackStack(null)
                    .commit();
        } else {
            RssDetailFragmet detailFragmet = (RssDetailFragmet) getSupportFragmentManager().findFragmentByTag(RssDetailFragmet.TAG);
            detailFragmet.loadUrl(rss.getLink());
        }
    }
}

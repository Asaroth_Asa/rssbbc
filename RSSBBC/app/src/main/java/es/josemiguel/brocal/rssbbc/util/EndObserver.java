package es.josemiguel.brocal.rssbbc.util;

import rx.Observer;

/**
 * Created by josemiguel on 15/02/14.
 */
public abstract class EndObserver<T> implements Observer<T> {
    @Override
    public void onCompleted() {
        onEnd();
    }

    @Override
    public void onError(Throwable throwable) {
        onEnd();
    }

    @Override
    public void onNext(T args) {
    }

    public abstract void onEnd();
}

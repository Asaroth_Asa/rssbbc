package es.josemiguel.brocal.rssbbc.datasource.api;

import com.squareup.okhttp.OkHttpClient;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import es.josemiguel.brocal.rssbbc.datasource.api.response.RssHandler;
import es.josemiguel.brocal.rssbbc.datasource.model.Rss;


/**
 * Implentacion de RssApi para consumir los rss de la BBC
 * <p/>
 * Created by josemiguel on 09/04/14.
 */
public class BBCApi implements RssApi {

    public static final String BBC_URL = "http://feeds.bbci.co.uk/news/rss.xml";

    private OkHttpClient okHttpClient;

    public BBCApi(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    @Override
    public List<Rss> getRss() throws IOException, XmlPullParserException {
        RssHandler rssHandler = new RssHandler();
        URL url1 = new URL(BBC_URL);
        HttpURLConnection connection = okHttpClient.open(url1);
        return rssHandler.parse(connection.getInputStream());
    }
}

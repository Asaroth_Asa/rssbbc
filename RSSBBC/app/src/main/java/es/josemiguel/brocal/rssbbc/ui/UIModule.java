package es.josemiguel.brocal.rssbbc.ui;

import android.app.Application;

import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.josemiguel.brocal.rssbbc.domain.RssProvider;
import es.josemiguel.brocal.rssbbc.ui.fragment.RssListFragment;
import es.josemiguel.brocal.rssbbc.ui.viewmodel.RssListViewModel;

@Module(
        injects = {
                RssListFragment.class
        },
        library = true,
        complete = false
)
public class UIModule {

    @Provides
    @Singleton
    Picasso providePicasso(Application context) {
        return Picasso.with(context);
    }

    @Provides
    @Singleton
    RssListViewModel provideRssListViewModel(RssProvider rssProvider) {
        return new RssListViewModel(rssProvider);
    }

}

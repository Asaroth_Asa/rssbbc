package es.josemiguel.brocal.rssbbc;

import android.app.Application;
import android.content.Context;

import dagger.ObjectGraph;

public class App extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        buildObjectGraphAndInject();
    }

    public void buildObjectGraphAndInject() {
        objectGraph = ObjectGraph.create(AppModule.Modules.list(this));
    }

    public void inject(Object o) {
        objectGraph.inject(o);
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }
}

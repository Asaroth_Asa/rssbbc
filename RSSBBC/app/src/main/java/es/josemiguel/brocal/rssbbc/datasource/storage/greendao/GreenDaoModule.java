package es.josemiguel.brocal.rssbbc.datasource.storage.greendao;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        library = true,
        complete = false
)
public class GreenDaoModule {

    @Provides
    SQLiteDatabase provideSQLiteOpenHelper(Application context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "rss-db", null);
        return helper.getWritableDatabase();
    }

    @Provides
    @Singleton
    DaoMaster provideDaoMaster(SQLiteDatabase db) {
        return new DaoMaster(db);
    }

    @Provides
    @Singleton
    DaoSession provideDaoSession(DaoMaster daoMaster) {
        return daoMaster.newSession();
    }

    @Provides
    RssDao provideRssDao(DaoSession daoSession) {
        return daoSession.getRssDao();
    }
}

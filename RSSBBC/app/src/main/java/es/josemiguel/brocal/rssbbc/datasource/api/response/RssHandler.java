package es.josemiguel.brocal.rssbbc.datasource.api.response;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.josemiguel.brocal.rssbbc.datasource.model.Rss;

/**
 * Created by josemiguel on 09/04/14.
 */
public class RssHandler {

    public List<Rss> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readRss(parser);
        } finally {
            in.close();
        }
    }

    private List<Rss> readRss(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Rss> entries = new ArrayList<Rss>();

        parser.require(XmlPullParser.START_TAG, null, "rss");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("channel")) {
                entries.addAll(readChannel(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }

    private List<Rss> readChannel(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Rss> rss = new ArrayList<Rss>();
        parser.require(XmlPullParser.START_TAG, null, "channel");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("item")) {
                rss.add(readItem(parser));
            } else {
                skip(parser);
            }
        }
        return rss;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    private Rss readItem(XmlPullParser parser) throws IOException, XmlPullParserException {
        Rss rss = new Rss();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss z", Locale.US);
        parser.require(XmlPullParser.START_TAG, null, "item");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                rss.setTitle(readText(parser));
            } else if (name.equals("description")) {
                rss.setDescription(readText(parser));
            } else if (name.equals("link")) {
                rss.setLink(readText(parser));
            } else if (name.equals("pubDate")) {
                try {
                    rss.setPublishDate(simpleDateFormat.parse(readText(parser)));
                } catch (ParseException e) {
                    rss.setPublishDate(new Date());
                }
            } else if (name.equals("media:thumbnail")) {
                rss.setCover(parser.getAttributeValue(null, "url"));
                skip(parser);
            } else {
                skip(parser);
            }
        }
        return rss;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

}

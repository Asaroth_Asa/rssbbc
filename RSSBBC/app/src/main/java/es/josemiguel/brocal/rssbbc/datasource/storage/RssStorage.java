package es.josemiguel.brocal.rssbbc.datasource.storage;

import java.util.List;

import es.josemiguel.brocal.rssbbc.datasource.model.Rss;

/**
 * Interfaz para persistir Rss. La persistencia dependera de la implementación.
 * <p/>
 * Created by josemiguel on 09/04/14.
 */
public interface RssStorage {

    void persistRss(List<Rss> rss);

    List<Rss> getAllRss();

    List<Rss> getAllRssOrderByPublishDate();
}

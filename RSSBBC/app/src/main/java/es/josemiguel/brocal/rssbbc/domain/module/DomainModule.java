package es.josemiguel.brocal.rssbbc.domain.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.josemiguel.brocal.rssbbc.datasource.api.RssApi;
import es.josemiguel.brocal.rssbbc.datasource.storage.RssStorage;
import es.josemiguel.brocal.rssbbc.domain.RssProvider;

@Module(
        library = true,
        complete = false
)
public class DomainModule {

    @Provides
    @Singleton
    RssProvider provideRssProvider(RssApi rssApi, RssStorage rssStorage) {
        return new RssProvider(rssApi, rssStorage);
    }

}

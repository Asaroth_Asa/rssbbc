package es.josemiguel.brocal.rssbbc.ui.base;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import es.josemiguel.brocal.rssbbc.App;

public class BaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.get(this).inject(this);
    }

}

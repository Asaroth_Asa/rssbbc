package es.josemiguel.brocal.rssbbc.datasource.module;

import android.app.Application;

import com.squareup.okhttp.OkHttpClient;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import es.josemiguel.brocal.rssbbc.datasource.api.BBCApi;
import es.josemiguel.brocal.rssbbc.datasource.api.RssApi;
import es.josemiguel.brocal.rssbbc.datasource.storage.RssStorage;
import es.josemiguel.brocal.rssbbc.datasource.storage.greendao.GreenDaoRssStorage;
import es.josemiguel.brocal.rssbbc.datasource.storage.greendao.RssDao;

@Module(
        library = true,
        complete = false
)
public class DataSourceModule {

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Application app) {
        return new OkHttpClient();
    }

    @Provides
    @Singleton
    RssApi provideBBCApi(OkHttpClient okHttpClient) {
        return new BBCApi(okHttpClient);
    }

    @Provides
    @Singleton
    RssStorage provideGreendaoRssStorage(RssDao rssDao) {
        return new GreenDaoRssStorage(rssDao);
    }

}

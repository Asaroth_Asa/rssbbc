package es.josemiguel.brocal.rssbbc.ui.viewmodel;

import java.util.List;

import es.josemiguel.brocal.rssbbc.datasource.model.Rss;
import es.josemiguel.brocal.rssbbc.domain.RssProvider;
import rx.Observer;
import rx.Subscription;

/**
 * Created by josemiguel on 10/04/14.
 */
public class RssListViewModel {

    private RssProvider rssProvider;

    public RssListViewModel(RssProvider rssProvider) {
        this.rssProvider = rssProvider;
    }

    /**
     * Descarga rss desde internet
     * @param observer
     * @return
     *  - Devuelve los rss descargados y los que ya fueron descargados
     */
    public Subscription populateRss(Observer<List<Rss>> observer) {
        return rssProvider.populateRssFromApi(observer);
    }

    /**
     * Devuelve los rss cacheados, en caso de que no haya nada descargado, descarga nuevos
     * rss y los guarda en la cache.
     *
     * @param observer
     * @return
     */
    public Subscription getRssFromCache(Observer<List<Rss>> observer) {
        return rssProvider.getRssFromCache(observer);
    }
}

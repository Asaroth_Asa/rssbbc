package es.josemiguel.brocal.rssbbc.domain;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

import es.josemiguel.brocal.rssbbc.datasource.api.RssApi;
import es.josemiguel.brocal.rssbbc.datasource.model.Rss;
import es.josemiguel.brocal.rssbbc.datasource.storage.RssStorage;
import es.josemiguel.brocal.rssbbc.util.EndObserver;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subscriptions.Subscriptions;
import rx.util.functions.Func1;

/**
 * Created by josemiguel on 10/04/14.
 */
public class RssProvider {

    private RssApi rssApi;
    private RssStorage storage;
    /**
     * Peticion de rss desde internet
     */
    private PublishSubject<List<Rss>> populateRssRequest;
    /**
     * Petición de rss dede la cache
     */
    private PublishSubject<List<Rss>> getRssRequest;

    public RssProvider(RssApi rssApi, RssStorage storage) {
        this.rssApi = rssApi;
        this.storage = storage;
    }

    /**
     * Descarga RSS desde internet, luego los persiste en sqlite.
     *
     * @param observer
     * @return
     */
    public Subscription populateRssFromApi(Observer<List<Rss>> observer) {
        if (populateRssRequest != null) {
            return populateRssRequest.subscribe(observer);
        }

        populateRssRequest = PublishSubject.create();
        Subscription subscription = populateRssRequest.subscribe(observer);
        populateRssRequest.subscribe(new EndObserver<List<Rss>>() {
            @Override
            public void onEnd() {
                populateRssRequest = null;
            }
        });

        Observable.create(new Observable.OnSubscribeFunc<List<Rss>>() {
            @Override
            public Subscription onSubscribe(Observer<? super List<Rss>> observer) {
                List<Rss> rssDownloaded = null;
                try {
                    rssDownloaded = rssApi.getRss();
                } catch (IOException ignore) {
                } catch (XmlPullParserException ignored) {
                }
                try {
                    if (rssDownloaded != null && rssDownloaded.size() > 0) {
                        storage.persistRss(rssDownloaded);
                    }
                    observer.onNext(storage.getAllRssOrderByPublishDate());
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }
                return Subscriptions.empty();
            }
        })
                .flatMap(new Func1<List<Rss>, Observable<Rss>>() {
                    @Override
                    public Observable<Rss> call(List<Rss> rss) {
                        return Observable.from(rss);
                    }
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(populateRssRequest);
        return subscription;
    }

    /**
     * Devuelve los rss cacheados, en caso de que no haya nada descargado, descarga nuevos
     * rss y los guarda en la cache.
     * @param observer
     * @return
     */
    public Subscription getRssFromCache(Observer<List<Rss>> observer) {
        if (getRssRequest != null) {
            return getRssRequest.subscribe(observer);
        }

        getRssRequest = PublishSubject.create();
        Subscription subscription = getRssRequest.subscribe(observer);
        getRssRequest.subscribe(new EndObserver<List<Rss>>() {
            @Override
            public void onEnd() {
                getRssRequest = null;
            }
        });

        Observable.create(new Observable.OnSubscribeFunc<List<Rss>>() {
            @Override
            public Subscription onSubscribe(Observer<? super List<Rss>> observer) {
                try {
                    List<Rss> rssCached = storage.getAllRssOrderByPublishDate();
                    if (rssCached == null || rssCached.size() == 0) {
                        rssCached = rssApi.getRss();
                        storage.persistRss(rssCached);
                    }
                    observer.onNext(rssCached);
                    observer.onCompleted();
                } catch (Exception e) {
                    observer.onError(e);
                }
                return Subscriptions.empty();
            }
        })
                .flatMap(new Func1<List<Rss>, Observable<Rss>>() {
                    @Override
                    public Observable<Rss> call(List<Rss> rss) {
                        return Observable.from(rss);
                    }
                })
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getRssRequest);
        return subscription;
    }
}

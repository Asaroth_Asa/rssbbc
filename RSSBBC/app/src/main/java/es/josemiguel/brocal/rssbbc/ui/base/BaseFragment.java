package es.josemiguel.brocal.rssbbc.ui.base;

import android.app.Activity;
import android.support.v4.app.Fragment;

import es.josemiguel.brocal.rssbbc.App;

public class BaseFragment extends Fragment {

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        App.get(getActivity()).inject(this);
    }
}

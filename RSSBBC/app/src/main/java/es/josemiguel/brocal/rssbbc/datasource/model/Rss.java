package es.josemiguel.brocal.rssbbc.datasource.model;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END

/**
 * Entity mapped to table RSS.
 */
public class Rss {

    private String title;
    private String link;
    private String description;
    private java.util.Date publishDate;
    private String cover;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public Rss() {
    }

    public Rss(String link) {
        this.link = link;
    }

    public Rss(String title, String link, String description, java.util.Date publishDate, String cover) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.publishDate = publishDate;
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public java.util.Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(java.util.Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}

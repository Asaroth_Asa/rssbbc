package es.josemiguel.brocal.rssbbc.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import es.josemiguel.brocal.rssbbc.R;

/**
 * Created by josemiguel on 09/04/14.
 */
public class RssDetailFragmet extends Fragment {

    public static final String TAG = "detail";

    private static final String URL = "url";
    private WebView webView;
    private String url;

    public static RssDetailFragmet newInstance(String link) {
        Bundle args = new Bundle();
        args.putString(URL, link);
        RssDetailFragmet f = new RssDetailFragmet();
        f.setArguments(args);
        return f;
    }

    public RssDetailFragmet() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            url = savedInstanceState.getString(URL);
        } else if (getArguments() != null) {
            url = getArguments().getString(URL);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(URL, url);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        webView = (WebView) inflater.inflate(R.layout.fragment_rss_detail, container, false);
        initViews(webView);
        return webView;
    }

    private void initViews(WebView v) {
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                // TODO mostrar un progressbar
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(view.getContext(), "Upss something was wrong! " + description, Toast.LENGTH_SHORT).show();
            }
        });
        webView.loadUrl(url);
    }

    public void loadUrl(String link) {
        url = link;
        webView.loadUrl(url);
    }

}

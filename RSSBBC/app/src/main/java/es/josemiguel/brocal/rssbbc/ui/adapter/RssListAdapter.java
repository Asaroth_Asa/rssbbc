package es.josemiguel.brocal.rssbbc.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.josemiguel.brocal.rssbbc.R;
import es.josemiguel.brocal.rssbbc.datasource.model.Rss;

/**
 * Created by josemiguel on 09/04/14.
 */
public class RssListAdapter extends BaseAdapter {

    List<Rss> rssList = new ArrayList<Rss>();
    Picasso picasso;
    private LayoutInflater inflater;

    public RssListAdapter(List<Rss> rssList, Picasso picasso) {
        this.rssList = rssList;
        this.picasso = picasso;
    }

    @Override
    public int getCount() {
        return rssList.size();
    }

    @Override
    public Rss getItem(int position) {
        return rssList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rssList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            if (inflater == null) {
                inflater = LayoutInflater.from(parent.getContext());
            }
            convertView = inflater.inflate(R.layout.rss_row, parent, false);
            convertView.setTag(new ViewHolder(convertView));
        }

        ViewHolder holder = ViewHolder.from(convertView.getTag());
        populateView(holder, getItem(position));

        return convertView;
    }

    public void swapRss(List<Rss> rss) {
        rssList = rss;
        notifyDataSetChanged();
    }

    private void populateView(ViewHolder holder, Rss item) {
        holder.title.setText(item.getTitle());
        holder.description.setText(item.getDescription());
        holder.pubDate.setText(item.getPublishDate().toString());
        picasso.load(item.getCover()).into(holder.cover);
    }

    private static class ViewHolder {
        private TextView title;
        private TextView description;
        private TextView pubDate;
        private ImageView cover;

        public ViewHolder(View v) {
            title = (TextView) v.findViewById(R.id.rss_row_title);
            description = (TextView) v.findViewById(R.id.rss_row_description);
            pubDate = (TextView) v.findViewById(R.id.rss_row_date);
            cover = (ImageView) v.findViewById(R.id.rss_row_cover);
        }

        public static ViewHolder from(Object tag) {
            return (ViewHolder) tag;
        }
    }
}
